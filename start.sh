#!/bin/bash

curl -XDELETE localhost:9200/temp_indiv
curl -XPUT localhost:9200/temp_indiv
curl -XDELETE localhost:9200/temp_team
curl -XPUT localhost:9200/temp_team
curl -XDELETE localhost:9200/temp_publications
IP=localhost:9200

SETTING_FILE=settings.json

GROUPE_NAME=temp_publications

INDEX_NAME=publication

MAPPING_FILE=publications.json

curl -XPUT $IP/$GROUPE_NAME

curl -XPOST $IP/$GROUPE_NAME/_close

curl -XPUT $IP/$GROUPE_NAME/_settings -H 'Content-Type: application/json' -d @$SETTING_FILE

curl -XPUT $IP/$GROUPE_NAME/$INDEX_NAME/_mapping -H 'Content-Type: application/json' -d @$MAPPING_FILE

curl -XPOST $IP/$GROUPE_NAME/_open

python3 crawler_new_pub.py
python3 get_tei_and_temp_in_elasticsearch.py
python3 update_teams_individuals.py
