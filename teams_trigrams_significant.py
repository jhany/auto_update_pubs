from elasticsearch import Elasticsearch
import configparser
import trigrams_with_significant_keywords
import re

def get_keywordsTeam(teamId, es) :
    config = configparser.RawConfigParser()
    config.read("ConfigFile.properties")
    index_pub = config.get("elasticsearch", "index_pub")
    query_keywords = {
                    "query": {
                        "match_all": {}
                    },
                    "aggregations": {
                        "affiliations": {
                            "nested": {
                                "path": "affiliations"
                            },
                            "aggs": {
                                "affiliationfilter": {
                                    "filter": {
                                        "term": {
                                            "affiliations.id": teamId
                                        }
                                    },
                                    "aggs": {
                                        "got_back": {
                                            "reverse_nested": {},
                                            "aggs": {
                                                "most_sig_words": {
                                                    "significant_terms": {
                                                        "field": "title_abstract_sign",
                                                        "size": 100
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
    }
    result = es.search(index=index_pub, body=query_keywords)
    result = result["aggregations"]["affiliations"]["affiliationfilter"]["got_back"]["most_sig_words"]["buckets"]
    return result

def replace_all(text, dic):
    for i, j in dic.items():
        text = text.replace(i, j)
    return text

def get_researchteam(acronym, es) :
    config = configparser.RawConfigParser()
    config.read("ConfigFile.properties")
    index_team = config.get("elasticsearch", "index_team")
    query = {"query" : {"match" : {"acronym" : acronym}}}
    return es.search(index=index_team, body=query)["hits"]["hits"]


def teams_new_topics(acronym) :
    config = configparser.RawConfigParser()
    config.read("ConfigFile.properties")
    es = Elasticsearch(config.get("elasticsearch", "ip"))
    index_team = config.get("elasticsearch", "index_team")
    index_pub = config.get("elasticsearch", "index_pub")

    # keywords = []
    result = get_researchteam(acronym, es)
    completeText = ""
    team = result[0]
    if "pubs" in team["_source"] :
        for publication in team["_source"]["pubs"]:
            # title_fr = publication["title_fr"]
            title_en = publication["title_en"]
            abstract_fr = ""
            abstract_en = ""
            # if "abstract_fr" in publication :
            #     abstract_fr = publication["abstract_fr"]
            # if "abstract_en" in publication :
            #     abstract_en = publication["abstract_en"]
            # completeText += abstract_fr + ' ' + title_fr + ' ' + title_en + ' ' + abstract_en + ' '
            completeText += title_en + ' ' + abstract_en + ' '
            completeText = replace_all(completeText, {"{" : "", "}" : "", '"' : "", "\n" : "", "\r" : "", "\\" : "", "’" : "", "‘" : "", "–" : "", "α" : "", "μ" : "", "≥" : "", "β" : "", "“" : "", "”" : "", "→" : "", "∧" : "", "∨" : "", "∊" : ""} )
            completeText = re.sub('[^A-Za-z]+',' ', completeText)
        maintopic_data = {}
        data_list = []
        if completeText != "" :
            keywords = get_keywordsTeam(team["_id"], es)
            data_list = trigrams_with_significant_keywords.create_significant_trigrams(es, completeText, index_pub, keywords)
        return data_list if data_list else ["empty"]
